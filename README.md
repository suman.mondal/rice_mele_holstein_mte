# Multi Trajectory Ehrenfest Method for Rice Mele Model

[Source code for the project : Phonon-induced breakdown of Thouless pumping in the Rice-Mele-Holstein model.](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.106.235118)

The folder "scripts" contains all the source code necessary to generate the results.
Ensure that the libraries are installed, which are imported in script "rice_mele_MTE.py", in the python environment before executing the code.
