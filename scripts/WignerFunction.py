import numpy as np

class Wigner_dist:
    def __init__(s):
        s.x_m, s.p_m = s.x_max(), s.p_max()
        s.x_list, s.p_list = s.distribution()

    def distribution(s):
        x_list = np.arange(-s.x_m, s.x_m, s.x_m / 200)
        p_list = np.arange(-s.p_m, s.p_m, s.p_m / 200)
        s.dim = (np.shape(x_list)[0], np.shape(p_list)[0])
        s.xp_list = []
        s.W0_list = []
        s.index = []
        s.W00 = np.zeros(s.dim)
        ii = 0
        for x in range(s.dim[0]):
            for p in range(s.dim[1]):
                s.xp_list.append((x_list[x], p_list[p], x, p))
                s.W00[x][p] = s.Wigner0(x_list[x], p_list[p])
                s.W0_list.append(s.W00[x][p])
                s.index.append(ii)
                ii += 1
        return x_list, p_list

    def x_max(s):
        P_s = s.Wigner0(0., 0.)
        x_n = 0.0
        for x_i in range(100000):
            x_n += 0.1
            P_n = s.Wigner0(x_n, 0.)
            if P_s / P_n >= 500: break
        return x_n

    def p_max(s):
        P_s = s.Wigner0(0., 0.)
        p_n = 0.0
        for p_i in range(100000):
            p_n += 0.1
            P_n = s.Wigner0(0., p_n)
            if P_s / P_n >= 500: break
        return p_n

    def Wigner0(s, xx, pp):
        return (1. / np.pi) * np.exp(-(xx ** 2.0)) * np.exp(-(pp ** 2.0))

    def coordinate(s,L):
        s.W0 = np.zeros(s.dim)
        s.W0_sample = []
        for kk in range(L):
            ii = np.random.choice(s.index, p=np.array(s.W0_list) / sum(s.W0_list))
            xp = s.xp_list[ii]
            s.W0_sample.append((xp[0], xp[1]))
            # print(xp)
            s.W0[xp[2]][xp[3]] += 1.0
        return s.W0_sample
