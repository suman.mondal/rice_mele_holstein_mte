from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import tensor_basis,spinless_fermion_basis_1d,boson_basis_1d # bases
from quspin.tools.measurements import obs_vs_time # calculating dynamics
from quspin.tools.Floquet import Floquet_t_vec # period-spaced time vector
import numpy as np # general math functions
import scipy as sc
import sys
import matplotlib.pyplot as plt # plotting library
import matplotlib.pyplot as plt
from scipy import integrate
import WignerFunction as WF0

L = 40
N = 1
J = 1.0
w0 = 0.1
gamma1 = 0.6
t_step = 0.01
T = 50.0
A_d = 0.75
A_D = 3.0
pump_off = 0.5*np.pi
store_avarage = True
ntr = 0. # trajectory index

'''=================================================================================================================='''
# command line argument
for i in range(1,len(sys.argv)):
        arg=sys.argv[i].lower()
        if arg=="-g":
                i+=1
                gamma1 = float(sys.argv[i])
        elif arg=="-w":
                i+=1
                w0 = float(sys.argv[i])
        elif arg=="-d":
                i+=1
                A_d = float(sys.argv[i])
        elif arg=="-D":
                i+=1
                A_D = float(sys.argv[i])
        elif arg=="-l":
                i+=1
                L = int(sys.argv[i])
        elif arg=="-t":
                i+=1
                T = float(sys.argv[i])
        elif arg=="-ntr":
                i+=1
                ntr = float(sys.argv[i])

time_last = T*50.0
t_trans = -T/16.0
Omega = 2.*np.pi/T

'''=================================================================================================================='''


def drive_hop_e(time):
	return -J*(1.0 + A_d*np.sin(Omega*time+pump_off))

def drive_hop_o(time):
	return -J*(1.0 - A_d*np.sin(Omega*time+pump_off))

def drive_pot_e(time):
	return (A_D/2.)*np.cos(Omega*time+pump_off)

def drive_pot_o(time):
	return -(A_D/2.)*np.cos(Omega*time+pump_off)

def drive_pot_x(time,x):
    if x%2 == 0: return (A_D/2.)*np.cos(Omega*time+pump_off) -gamma*np.sqrt(2.0)*x_l[x]
    else: return -(A_D/2.)*np.cos(Omega*time+pump_off) -gamma*np.sqrt(2.0)*x_l[x]

def transient(time):
    if time<0: return (-1./(2.*t_trans))*time**2.+time
    else: return time

def FT(k_val, ri, rj, cor_ij):
    rd = float(rj-ri)
    return (np.exp(-1.J*rd*k_val))*cor_ij

def evolve_system(psi_list, Ham, obs):
    new_psi_list = []
    exp_H = sc.linalg.expm(-1.J * t_step * Ham)
    for psi_i in psi_list: new_psi_list.append(exp_H.dot(psi_i))
    obs_dict = {}
    for key in obs.keys():
        opr = obs[key]
        obr_val = 0.0
        for psi_i in new_psi_list: obr_val += psi_i.conj().dot(opr.dot(psi_i))
        # print(key,obr_val)
        obs_dict[key] = obr_val
    return new_psi_list, obs_dict

def expectation(psi_list, obss):
    obs_dict = {}
    for key in obss.keys():
        opr = obss[key]
        obr_val = 0.0
        for psi_i in psi_list: obr_val += psi_i.conj().dot(opr.dot(psi_i))
        # print(key,obr_val)
        obs_dict[key] = obr_val
    return obs_dict

def evolve_env_uncoupled(time_step):
    for l in range(L):
        x_l[l] = x_l[l] * np.cos(w0 * time_step / 2.) - p_l[l] * np.sin(w0 * time_step / 2.)
        p_l[l] = x_l[l] * np.sin(w0 * time_step / 2.) + p_l[l] * np.cos(w0 * time_step / 2.)
    return x_l, p_l

def evolve_env_coupled(time_step,num_of_particle):
    for l in range(L):
        p_l[l] = p_l[l] + time_step*np.sqrt(2)*gamma*num_of_particle[l]/ 2.
    return p_l

'''=================================================================================================================='''


basis = spinless_fermion_basis_1d(L, Nf=N)

even_hop_f_right = [[drive_hop_e(transient(t_trans)), i, (i + 1)] for i in range(0, L - 1, 2)]  # f hopping right, OBC
even_hop_f_left = [[-drive_hop_e(transient(t_trans)), i, (i + 1)] for i in range(0, L - 1, 2)]  # f hopping left

odd_hop_f_right = [[drive_hop_o(transient(t_trans)), i, (i + 1)] for i in range(1, L - 2, 2)]+[[drive_hop_o(transient(t_trans)), L-1, 0]]  # f hopping right, OBC
odd_hop_f_left = [[-drive_hop_o(transient(t_trans)), i, (i + 1)] for i in range(1,L - 2, 2)]+[[-drive_hop_o(transient(t_trans)), L-1, 0]]  # f hopping left

even_pot = [[drive_pot_e(transient(t_trans)), i] for i in range(0, L, 2)]
odd_pot = [[drive_pot_o(transient(t_trans)), i] for i in range(1, L, 2)]

static = [["+-", even_hop_f_right + odd_hop_f_right],  # fermions hop left
          ["-+", even_hop_f_left + odd_hop_f_left],  # fermions hop right
          ["n", even_pot + odd_pot]
         ]

dynamic = []

H_RM = hamiltonian(static, dynamic, basis=basis, check_symm=False, check_pcon=False)
E,V=H_RM.eigh()
V_0_0 = 1.*V.T[0]
H_hop = hamiltonian([["+-", even_hop_f_right + odd_hop_f_right], ["-+", even_hop_f_left + odd_hop_f_left]],
                    dynamic, basis=basis, check_symm=False, check_pcon=False).toarray()

'''=================================================================================================================='''

even_hop_f_right = [[1., i, (i + 1)] for i in range(0, L - 1, 2)]  # f hopping right, OBC
even_hop_f_left = [[-1., i, (i + 1)] for i in range(0, L - 1, 2)]  # f hopping left
static = [["+-", even_hop_f_right],  # fermions hop left
          ["-+", even_hop_f_left],   # fermions hop right
         ]
dynamic = []
H_hop_even = hamiltonian(static, dynamic, basis=basis, check_symm=False, check_pcon=False).toarray()

odd_hop_f_right = [[1., i, (i + 1)] for i in range(1, L - 2, 2)]+[[1., L-1, 0]]  # f hopping right, OBC
odd_hop_f_left = [[-1., i, (i + 1)] for i in range(1,L - 2, 2)]+[[-1., L-1, 0]]  # f hopping left
static = [["+-", odd_hop_f_right],  # fermions hop left
          ["-+", odd_hop_f_left],  # fermions hop right
         ]
H_hop_odd = hamiltonian(static, dynamic, basis=basis, check_symm=False, check_pcon=False).toarray()

no_checks = dict(check_herm=False,check_symm=False,check_pcon=False)
ststic =  [
          ["n", [[1., i] for i in range(0, L, 2)]]
          ]
H_pot_even = hamiltonian(ststic, dynamic, basis=basis,dtype=np.float64,**no_checks).toarray()
ststic =  [
          ["n", [[1., i] for i in range(1, L, 2)]]
          ]
H_pot_odd = hamiltonian(ststic, dynamic, basis=basis,dtype=np.float64,**no_checks).toarray()

'''=================================================================================================================='''
obs = {}
no_checks = dict(check_herm=False,check_symm=False,check_pcon=False)
n_list = [hamiltonian([["n",[[1.0,i]]]],[],basis=basis,dtype=np.float64,**no_checks).toarray() for i in range(L)]
for l in range(L):
    obs[(('n',l),)] = n_list[l]
corr = {}
for ii in range(1,3):
    for jj in range(1,3):
        # if ii == jj: continue
        corr[(('cd', ii), ('c', jj))] = hamiltonian([["+-", [[1.0,  ii, jj]]]], [], basis=basis, dtype=np.float64, **no_checks).toarray()
# print(corr[(('cd', 1), ('c', 2))])
'''=================================================================================================================='''
# print(t_trans+t_step, time_last, int((time_last-t_trans)/t_step))
time_list = np.linspace(t_trans+t_step, time_last, int((time_last-t_trans)/t_step)).tolist()

n_e_mat_dict = {}
n_kA_mat_dict = {}
n_kB_mat_dict = {}
occ_up = {}
occ_dn = {}
n_ph_mat_dict = {}
ph_xi_mat_dict = {}
ph_pi_mat_dict = {}
Qo_dict = {}
DE_dict ={}
enr_dict = {}
no_of_stapes = 10000
time_sample = int(len(time_list)/no_of_stapes)
N_sample = 1 # number of trajectories
wf0 = WF0.Wigner_dist()
for traj in range(N_sample):
    xp_list = wf0.coordinate(L)
    # print(traj)
    x_l = [xp[0] for xp in xp_list]
    p_l = [xp[1] for xp in xp_list]
    # x_l = [0. for xp in xp_list]
    # p_l = [0. for xp in xp_list]

    psi_list_t = []
    for psi_ind in range(int(L/2)): psi_list_t.append(V.T[psi_ind])
    bos_expt = {}
    for key in obs.keys(): bos_expt[key] = sum([psi_i.conj().dot(obs[key].dot(psi_i)) for psi_i in psi_list_t])
    expt_n = [bos_expt[(('n',l),)] for l in range(L)]

    n_ph_list = []
    ph_xi_list = []
    ph_pi_list = []
    n_e_list = []
    n_kA_list = []
    n_kB_list = []
    occ_up_list = []
    occ_dn_list = []
    curro = []
    Qo = []
    DE =[]
    enr_list = []
    act_time_list = []
    tt_ii = 0.0
    for t_now in time_list:
        if t_now < 0.0: gamma = 0.0
        else: gamma = gamma1#*xp[0]
        tt_ii += 1.0

        x_l, p_l = evolve_env_uncoupled(t_step)
        p_l = evolve_env_coupled(t_step, expt_n)

        Ham = drive_hop_e(transient(t_now)) * H_hop_even + drive_hop_o(transient(t_now)) * H_hop_odd
        # obs['E_k'] = 1.*Ham
        obs['E_RM'] = Ham + drive_pot_e(transient(t_now))*H_pot_even + drive_pot_o(transient(t_now))*H_pot_odd
        for l in range(L):
            Ham += drive_pot_x(transient(t_now), l) * n_list[l]
        obs['E'] = Ham
        psi_list_t, bos_expt = evolve_system(psi_list_t, Ham, obs)
        # overlap.append(V_0_0.dot(psi_list_t[0]))
        expt_n = [bos_expt[(('n', l),)] for l in range(L)]
        # print(bos_expt)

        p_l = evolve_env_coupled(t_step, expt_n)
        x_l, p_l = evolve_env_uncoupled(t_step)

        if tt_ii % time_sample == 0:  # observables 
            # print(t_now)
            act_time_list.append(t_now)
            n_e_list.append(expt_n)
            n_ph_list.append(sum([(x_l[l] ** 2. + p_l[l] ** 2. -0.)/2. for l in range(L)])/L)
            ph_xi_list.append([x_l[l] for l in range(L)])
            ph_pi_list.append([p_l[l] for l in range(L)])
            corr_mat = expectation(psi_list_t,corr)
            curro.append(-2. * drive_hop_o(transient(t_now)) * (corr_mat[(('cd', 2), ('c', 1))].imag))
            Qo.append(integrate.simpson(np.array(curro), np.array(act_time_list)))
            Enr, Vec = sc.linalg.eigh(obs['E'])
            DE.append(Enr[int(L/2)]-Enr[int(L/2-1)])
            enr_list.append([Enr[l] for l in range(L)])
            oc_d = np.dot(np.asarray(psi_list_t), Vec.T[0:int(L/2)].T)
            oc_u = np.dot(np.asarray(psi_list_t), Vec.T[int(L/2):int(L)].T)
            occ_dn_list.append(np.sum(np.square(np.abs(oc_d))))
            occ_up_list.append(np.sum(np.square(np.abs(oc_u))))


    n_e_mat_dict[traj] = np.asarray(n_e_list)
    n_kA_mat_dict[traj] = np.asarray(n_kA_list)
    n_kB_mat_dict[traj] = np.asarray(n_kB_list)
    occ_up[traj] = np.asarray(occ_up_list)
    occ_dn[traj] = np.asarray(occ_dn_list)
    n_ph_mat_dict[traj] = np.asarray(n_ph_list)
    Qo_dict[traj] = np.asarray(Qo)
    DE_dict[traj] = np.asarray(DE)
    enr_dict[traj] = np.asarray(enr_list)
    ph_xi_mat_dict[traj] = np.asarray(ph_xi_list)
    ph_pi_mat_dict[traj] = np.asarray(ph_pi_list)

    # plt.plot(act_time_list, Qo, '-', alpha=0.7)


obs_expt_dict = {'n_e': n_e_mat_dict, 'n_kA': n_kA_mat_dict, 'n_kB': n_kB_mat_dict, 'occ_up':occ_up,'occ_dn':occ_dn, 'n_ph': n_ph_mat_dict, 'ph_xi':ph_xi_mat_dict, 'ph_pi':ph_pi_mat_dict, 'Qo': Qo_dict, 'dE': DE_dict, 'enr':enr_dict}
final_dict = {}
if store_avarage:
    for key in obs_expt_dict.keys():
        # print(list(obs_expt_dict[key].values()))
        final_dict[key] = sum(list(obs_expt_dict[key].values()))/float(N_sample)
else:
    final_dict = obs_expt_dict
final_dict['t_list'] = np.asarray(act_time_list)
final_dict['N_traj'] = N_sample

import pickle #  save data
file_name = '/scratch/users/mondal1/pumping/output_MTE/output_rand_occ_new_ntr' + str(ntr) + '_L_' + str(L) + '_d_' + str(A_d) + '_D_' + str(A_D) + '_g_' +\
            str(gamma) + '_w_' + str(w0) + '_T_' + str(T) + '_Tt_' + str(t_trans) + '.pkl'
#file_name = 'output_MTE/output_rand_occ_ntr' + str(ntr) + '_L_' + str(L) + '_d_' + str(A_d) + '_D_' + str(A_D) + '_g_' +\
#            str(gamma) + '_w_' + str(w0) + '_T_' + str(T) + '_Tt_' + str(t_trans) + '.pkl'
output = open(file_name, 'wb')
pickle.dump(final_dict, output)
output.close()
'''=================================================================================================================='''

